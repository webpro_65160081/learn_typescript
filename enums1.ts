enum CadinalDirections {
    North = 1,
    East, //-------> 2
    South, //-------> 3
    West //-------> 4
}

let currentDirection = CadinalDirections.North;
console.log(currentDirection);

enum CadinalDirections2 {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection2 = CadinalDirections2.North;
console.log(currentDirection2);
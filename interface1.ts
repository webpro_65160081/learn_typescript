interface Rectangle {
    width: number;
    height: number;
}

interface ColorRectangle extends Rectangle {
    color: string;
}
const rectangle: Rectangle = {
    width: 20,
    height: 50
}

console.log(rectangle);


const colorRectangle:ColorRectangle = {
    width: 20,
    height: 50,
    color: "green"
}

console.log(colorRectangle);
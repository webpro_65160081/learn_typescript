const car: {type: string, model: string, year: number} = {
    type: "Toyota",
    model: "Corolla",
    year: 2009
};

console.log(car);

// Optional property
const car2: {type: string, model: string, year?: number} = {
    type: "Toyota",
    model: "collora"
};

console.log(car2);